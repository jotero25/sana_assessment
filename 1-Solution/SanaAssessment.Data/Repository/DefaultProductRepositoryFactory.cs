﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SanaAssessment.Data.Repository
{
    public class DefaultProductRepositoryFactory : IProductRepositoryFactory
    {
        public IProductRepository GetImplementation(RepositoryType type)
        {
            switch (type)
            {
                case RepositoryType.InMemory:
                    return new MemoryProductRepository();
                case RepositoryType.SqlCe:
                    return new Sql.SqlCeProductRepository();
            }

            throw new NotImplementedException(string.Format("Repository of type '{0}' has not been implemented", type));
        }
    }
}
