﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SanaAssessment.Data.Model;

namespace SanaAssessment.Data.Repository.Sql
{
    public class SqlCeProductRepository : IProductRepository
    {
        ProductsContext _context;

        public SqlCeProductRepository()
        {
            _context = new ProductsContext("SQLCE");
        }

        public void Delete(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException("product");
            }

            _context.DeleteById(product.Id);
            //Commit();
        }

        public IEnumerable<Product> Get()
        {
            return _context.GetAll();
        }

        public Product GetById(int id)
        {
            return _context.GetById(id);
        }

        public void Insert(Product product)
        {
            _context.Create(product);
        }

        public void Update(Product product)
        {
            _context.Update(product);
        }
    }
}
