﻿using SanaAssessment.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SanaAssessment.Data.Repository.Sql
{
    public class ProductsContext 
    {
        private string _cnnStringName;
        private string _cnnString;
        public ProductsContext(string connectionStringName)
        {
            _cnnStringName = connectionStringName;
            Initialize();
        }

        public IEnumerable<Product> GetAll()
        {
            using (SqlCeConnection cnn = GetConnection())
            {
                using (SqlCeCommand cmd = new SqlCeCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandText = "SELECT Id, Title, Price FROM Products";
                    using (SqlCeDataReader reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            yield return new Product
                            {
                                Id = reader.GetInt32(0),
                                Title = reader.GetString(1),
                                Price = reader.GetDecimal(2)
                            };
                        }
                    }
                }
            }
        }
        public Product GetById(int id)
        {
            using (SqlCeConnection cnn = GetConnection())
            {
                using (SqlCeCommand cmd = new SqlCeCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandText = "SELECT Title, Price FROM Products WHERE Id = @Id";
                    cmd.Parameters.AddWithValue("@Id", id);
                    using (SqlCeDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new Product
                            {
                                Id = id,
                                Title = reader.GetString(0),
                                Price = reader.GetDecimal(1)
                            };
                        }
                    }
                }
            }
            return null;
        }

        public void DeleteById(int id)
        {
            using (SqlCeConnection cnn = GetConnection())
            {
                using (SqlCeCommand cmd = new SqlCeCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandText = "DELETE FROM Products WHERE Id = @Id";
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Create(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException("product");
            }

            using (SqlCeConnection cnn = GetConnection())
            {
                using (SqlCeCommand cmd = new SqlCeCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandText = "INSERT INTO Products (Title, Price) VALUES (@Title, @Price);";
                    cmd.Parameters.AddWithValue("@Title", product.Title);
                    cmd.Parameters.AddWithValue("@Price", product.Price);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    cmd.CommandText = "SELECT @@IDENTITY";
                    product.Id = (int)(decimal)cmd.ExecuteScalar();
                }
            }
        }
        public void Update(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException("product");
            }

            using (SqlCeConnection cnn = GetConnection())
            {
                using (SqlCeCommand cmd = new SqlCeCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandText = "UPDATE Products SET Title = @Title, Price = @Price WHERE Id = @Id";
                    cmd.Parameters.AddWithValue("@Id", product.Id);
                    cmd.Parameters.AddWithValue("@Title", product.Title);
                    cmd.Parameters.AddWithValue("@Price", product.Price);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void Initialize()
        {
            try
            {
                string cnnString = GetConnectionString();
                SqlCeConnectionStringBuilder cnnBuilder = new SqlCeConnectionStringBuilder(cnnString);
                string path = cnnBuilder.DataSource;
                path = path.Replace("|DataDirectory|", AppDomain.CurrentDomain.GetData("DataDirectory").ToString());
                if (!System.IO.File.Exists(path))
                {
                    using (SqlCeEngine eng = new SqlCeEngine(cnnString))
                    {
                        eng.CreateDatabase();
                    }

                    using (SqlCeConnection cnn = GetConnection())
                    {
                        using (SqlCeCommand cmd = new SqlCeCommand())
                        {
                            cmd.Connection = cnn;
                            cmd.CommandText = @"
                                CREATE TABLE Products (
                                    [Id] INT IDENTITY(1,1) NOT NULL,
                                    [Title] NVARCHAR(256) NOT NULL CONSTRAINT UK_Products_Name UNIQUE,
                                    [Price] NUMERIC(19,5) NOT NULL CONSTRAINT DF_Products_Price DEFAULT (0),
                                    PRIMARY KEY (Id))";
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to initialize SqlCe Database due: {0}", ex.Message));
            }

        }
        private string GetConnectionString()
        {
            if (_cnnString == null)
            {
                var cnnStringObject = System.Configuration.ConfigurationManager.ConnectionStrings[_cnnStringName];
                if (cnnStringObject != null)
                {
                    _cnnString = cnnStringObject.ConnectionString;
                }
            }

            if (string.IsNullOrEmpty(_cnnString))
            {
                throw new ApplicationException(string.Format("Database connection string has not been properly setup. Make sure the web.config file contains a Connection String named '{0}' before continue.", _cnnStringName));
            }
            return _cnnString;
        }

        private SqlCeConnection GetConnection()
        {
            if (_cnnString == null)
            {
                var cnnStringObject = System.Configuration.ConfigurationManager.ConnectionStrings[_cnnStringName];
                if (cnnStringObject != null)
                {
                    _cnnString = cnnStringObject.ConnectionString;
                }
            }

            if (string.IsNullOrEmpty(_cnnString))
            {
                throw new ApplicationException(string.Format("Database connection string has not been properly setup. Make sure the web.config file contains a Connection String named '{0}' before continue.", _cnnStringName));
            }
            
            SqlCeConnection cnn = new SqlCeConnection(_cnnString);
            if (cnn.State != System.Data.ConnectionState.Open)
            {
                cnn.Open();
            }
            return cnn;
        }
    }
}
