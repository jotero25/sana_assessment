﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SanaAssessment.Data.Model;

namespace SanaAssessment.Data.Repository
{
    public class MemoryProductRepository : IProductRepository
    {
        private List<Product> _products;
        private int _maxId;

        public MemoryProductRepository()
        {
            var products = System.Web.HttpContext.Current.Session["Products"] as List<Product>;
            _products = products ?? new List<Product>();
            _maxId = _products.Count > 0 ? _products.Max(p => p.Id) : 0;
        }

        public void Delete(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException("product");
            }

            _products.Remove(GetById(product.Id));
            Commit();
        }

        public IEnumerable<Product> Get()
        {
            return _products.Select(p => {
                return new Product
                {
                    Id = p.Id,
                    Title = p.Title,
                    Price = p.Price
                };
            });
        }

        public Product GetById(int id)
        {
            return _products.FirstOrDefault(p => p.Id == id);
        }

        public void Insert(Product product)
        {
            validate(product);
            if (product.Id != 0) {
                throw new InvalidOperationException("Id must be zero for insert operations, it will be autoassigned");
            }

            _maxId++;
            //Create a 'clone' so it is updatable only using the Update method
            var p = new Product
            {
                Id = _maxId,
                Title = product.Title,
                Price = product.Price
            };
            _products.Add(p);
            product.Id = _maxId;
            Commit();
        }

        public void Update(Product product)
        {
            validate(product);
            var p = GetById(product.Id);
            if (p == null)
            {
                throw new InvalidOperationException(string.Format("Product with id '{0}' does not exists", product.Id));
            }

            p.Title = product.Title;
            p.Price = product.Price;
            Commit();
        }

        private void validate(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException("product");
            }

            if (string.IsNullOrEmpty(product.Title) || _products.Any(p => p.Title == product.Title && p.Id != product.Id))
            {
                throw new InvalidOperationException("Title is empty or already exists.");
            }
        }

        private void Commit()
        {
            System.Web.HttpContext.Current.Session["Products"] = _products;
        }
    }
}
