﻿using SanaAssessment.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SanaAssessment.Data.Repository
{
    public interface IProductRepository
    {
        IEnumerable<Product> Get();
        Product GetById(int id);
        void Insert(Product product);
        void Update(Product product);
        void Delete(Product product);
    }
}
