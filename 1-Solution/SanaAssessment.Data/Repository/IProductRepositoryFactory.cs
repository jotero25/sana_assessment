﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SanaAssessment.Data.Repository
{
    public enum RepositoryType { InMemory = 0, SqlCe }
    public interface IProductRepositoryFactory
    {
        IProductRepository GetImplementation(RepositoryType type);
    }
}
