﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SanaAssessment.Data.Model
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class Product
    {
        public int Id { get; set; }
        [Required]
        [StringLength(256, ErrorMessage = "Title cannot be larger than 256 characters")]
        public string Title { get; set; }
        [Range(0, double.MaxValue, ErrorMessage = "Price cannot be negative")]
        public decimal Price { get; set; }
    }
}
