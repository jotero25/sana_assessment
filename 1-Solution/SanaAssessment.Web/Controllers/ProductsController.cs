﻿using SanaAssessment.Data.Model;
using SanaAssessment.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanaAssessment.Web.Controllers
{
    public class ProductsController : Controller
    {
        IProductRepositoryFactory _factory;
        RepositoryType _storageType;
        public ProductsController(IProductRepositoryFactory repositoryFactory)
        {
            _factory = repositoryFactory;
        }
        //
        // GET: /Products/
        public ActionResult Index()
        {
            var products = _factory.GetImplementation(_storageType).Get();
            return View(products);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Title,Price")]Product product)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _factory.GetImplementation(_storageType).Insert(product);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ExceptionMessage = ex.Message;
                }
            }
            return View(product);
        }

        public ActionResult Edit(int id)
        {
            var product = _factory.GetImplementation(_storageType).GetById(id);
            if (product == null)
            {
                return new HttpNotFoundResult(string.Format("Product with id = {0} not found", id));
            }
            return View(product);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, [Bind(Include = "Title,Price")]Product product)
        {
            if (ModelState.IsValid)
            {
                product.Id = id;
                try
                {
                    _factory.GetImplementation(_storageType).Update(product);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ExceptionMessage = ex.Message;
                }
            }
            return View(product);
        }

        public ActionResult Delete(int id)
        {
            var product = _factory.GetImplementation(_storageType).GetById(id);
            if (product == null)
            {
                return new HttpNotFoundResult(string.Format("Product with id = {0} not found", id));
            }
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDelete(int id)
        {
            var product = _factory.GetImplementation(_storageType).GetById(id);
            try
            {
                _factory.GetImplementation(_storageType).Delete(product);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ExceptionMessage = ex.Message;
            }
            return View(product);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            ResolveStorageType();
        }

        private void ResolveStorageType()
        {
            HttpCookie cookie = Request.Cookies["StorageType"];
            if (cookie != null)
            {
                RepositoryType stType;
                if (!Enum.TryParse<RepositoryType>(cookie.Value, out stType))
                {
                    stType = RepositoryType.InMemory;
                }
                _storageType = stType;
            }

            ViewBag.StorageType = _storageType;
        }
    }
}