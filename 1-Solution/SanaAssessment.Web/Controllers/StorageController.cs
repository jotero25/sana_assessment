﻿using SanaAssessment.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanaAssessment.Web.Controllers
{
    public class StorageController : Controller
    {
        // GET: Storage
        public ActionResult Change(RepositoryType type)
        {
            var cookie = new HttpCookie("StorageType", type.ToString());
            cookie.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(cookie);

            return Redirect("/");
        }
    }
}