--To Store Products
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE ID = object_id(N'Products') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1)
CREATE TABLE Products (
    [Id]            INT IDENTITY(1,1) NOT NULL
	, [Name]          VARCHAR(256) NOT NULL CONSTRAINT UK_Products_Name UNIQUE
	, [Description]	  INT NOT NULL CONSTRAINT DF_Products_Description DEFAULT ('')
	, [Quantity]      INT NOT NULL CONSTRAINT DF_Products_Quantity DEFAULT (0) 
	, [Price]         NUMERIC(19,5) NOT NULL CONSTRAINT DF_Products_Price DEFAULT (0) 
	, PRIMARY KEY (Id)
  , CONSTRAINT CHK_Products_Quantity_Positive CHECK (Quantity >= 0)
  , CONSTRAINT CHK_Products_Price_Positive CHECK (Price >= 0)
) ON [PRIMARY]
GO

--To Store Categories
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE ID = object_id(N'Categories') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1)
CREATE TABLE Categories (
    [Id]            INT IDENTITY(1,1) NOT NULL
	, [Name]          VARCHAR(256) NOT NULL CONSTRAINT UK_Categories_Name UNIQUE
	, [Description]	  INT NOT NULL CONSTRAINT DF_Categories_Description DEFAULT ('')
	, PRIMARY KEY (Id)
) ON [PRIMARY]
GO

--To manage many to many Products - Categories Relationship
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE ID = object_id(N'ProductCategories') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1)
CREATE TABLE ProductCategories (
	  [ProductId]     INT NOT NULL
	, [CategoryId]	  INT NOT NULL
	, PRIMARY KEY (ProductId, CategoryId)
  , CONSTRAINT FK_ProductCategories_ProductId_Products_Id FOREIGN KEY ( ProductId ) REFERENCES Products (Id) ON DELETE CASCADE
  , CONSTRAINT FK_ProductCategories_CategoryId_Categories_Id FOREIGN KEY ( CategoryId ) REFERENCES Categories (Id)
) ON [PRIMARY]
GO

--To Store Customers
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE ID = object_id(N'Customers') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1)
CREATE TABLE Customers (
    [Id]            INT IDENTITY(1,1) NOT NULL
	, [Name]          VARCHAR(256) NOT NULL
	, [Address]	      VARCHAR(256) NOT NULL CONSTRAINT DF_Customers_Address DEFAULT ('')
	, PRIMARY KEY (Id)
) ON [PRIMARY]
GO

--To Store Orders
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE ID = object_id(N'Orders') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1)
CREATE TABLE Orders (
    [Id]            INT IDENTITY(1,1) NOT NULL
	, [CustomerId]    INT NOT NULL
	, [ShipAddress]	  VARCHAR(256)
	, [DatePlaced]	  DATETIME NOT NULL CONSTRAINT DF_Orders_DatePlaced DEFAULT(getdate())
	, [TotalPrice]	  NUMERIC(19,5) NOT NULL CONSTRAINT DF_Orders_TotalPrice DEFAULT(0) --It must be a summary of Price on OrderDetails
	, PRIMARY KEY (Id)
  , CONSTRAINT FK_Orders_CustomerId_Customers_Id FOREIGN KEY ( CustomerId ) REFERENCES Customers (Id)
) ON [PRIMARY]
GO

--To Store Orders
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE ID = object_id(N'OrderDetails') AND OBJECTPROPERTY(ID, N'IsUserTable') = 1)
CREATE TABLE OrderDetails (
    [Id]            INT IDENTITY(1,1) NOT NULL
	, [OrderId]       INT NOT NULL
	, [ProductId]     INT NOT NULL
	, [Quantity]      INT NOT NULL
	, [UnitPrice]     NUMERIC(19,5) NOT NULL CONSTRAINT DF_OrderDetails_UnitPrice DEFAULT (0) --Must me assigned from Products[Price] when the record is created
	, [TotalPrice]    AS [Quantity] * [UnitPrice]
	, PRIMARY KEY (Id)
  , CONSTRAINT FK_OrderDetails_OrderId_Orders_Id FOREIGN KEY ( OrderId ) REFERENCES Orders (Id) ON DELETE CASCADE
  , CONSTRAINT FK_OrderDetails_CustomerId_Customers_Id FOREIGN KEY ( CustomerId ) REFERENCES Customers (Id)
  , CONSTRAINT CHK_OrderDetails_Quantity_Positive CHECK (Quantity > 0)
  , CONSTRAINT CHK_OrderDetails_UnitPrice_Positive CHECK (UnitPrice >= 0)
) ON [PRIMARY]
GO

